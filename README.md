# MsgSync Server

> This is the backend application to the MsgSync Progressive Web Application.

## About
This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.
The server models "subscriptions" to push notification clients, "files" that user upload, and "shares" of text (String)

This application is intended for use in a self hosted private setting.
The database [NeDB](https://github.com/louischatriot/nedb/) is an Embedded persistent or in memory database with no binary dependency.
It can handle enough concurrent users for a self hosted environment, however; it will not allow the application to scale with more instances.

## Application Overview
### Interaction walkthrough
- User is created in ./setup.js
- User logs in from the login form which assigns them a JWT token.  This is a collaboration between the _users_ service and feathersjs authentication module.
- User submits a bit of text through the _shares_ service.
- User submits a binary file through the _files_ service.  
- User navigates to /list to retrieve an ordered list of their submissions.
- User can copy text to clipboard and download uploaded files
- User can delete shares or files


- Notifications
- Upon creation of a share or file the _send-notification_ hook will trigger and send a push notification to all subscribed devices for that user.


### Services
## users
1 unique email address is a user with an associated JWT token to authenticate their api use

## subscriptions
manages N number of devices for a user.  These devices will be sent push notifications upon creation of a share or file

## shares
Shares are simple text that need to be shared between devices

## files
Files are not protected with authentication, this is by design. 
The purpose of this app was to quickly share text/files and it was ideal to publicly share the file url.
It would certainly be a good stretch goal to allow public and private files.
Has an _isImage_ metadata property to aid in display on client devices.

### Push Notifications
I wanted to note that it feels icky using the Progressive Web App implementation of Push Notifications as I feel that a piece of this application is not self hosted anymore.
Personally it was a sacrifice I was willing to take.  I would like to see end-to-end encryption of the messages that went through the web push api.  
Even better would be an implementation that doesn't rely on browsers infrastructure.

# Development Notes
## Resources

Fetch alternatives:

https://swizec.com/blog/a-tiny-es6-fetch-wrapper/swizec/7177

https://github.com/niftylettuce/frisbee


## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/msgSyncServer; npm install
    ```

3. Start your app

    ```
    npm start
    ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers help                           # Show all commands
```

## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).

## Changelog

__0.1.0__

- Initial release

## License

Copyright (c) 2018

Licensed under the [MIT license](LICENSE).
