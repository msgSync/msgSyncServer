const app = require('../../src/app')

describe('\'appSettings\' service', () => {
  it('registered the service', () => {
    const service = app.service('app-settings')
    expect(service).toBeTruthy()
  })
})
