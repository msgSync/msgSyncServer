const app = require('../../src/app');

describe('\'admin-settings\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/admin-settings');
    expect(service).toBeTruthy();
  });
});
