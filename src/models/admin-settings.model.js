const NeDB = require('nedb')
const path = require('path')
const logger = require('../logger')

module.exports = function (app) {
  const dbPath = app.get('nedb')
  const Model = new NeDB({
    filename: path.join(dbPath, 'admin-settings.db'),
    autoload: true
  })

  Model.ensureIndex({fieldName: '_id', unique: true},
    (err) => logger.error(err))

  return Model
}
