const NeDB = require('nedb')
const path = require('path')
const logger = require('../logger')

module.exports = function (app) {
  const dbPath = app.get('nedb')
  const Model = new NeDB({
    filename: path.join(dbPath, 'users.db'),
    autoload: true
  })

  Model.ensureIndex({ fieldName: 'email', unique: true },
    (err) => logger.error(err))

  return Model
}
