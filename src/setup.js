const setup = async (app) => {

  // Set application Secret
  if (!process.env.SECRET) {
    console.warn('The application is not secure.  Sent the environment variable SECRET')
  } else {
    const authConfig = app.get('authentication')
    // console.log('authConfig', authConfig)
    app.set('authentication', {
      ...authConfig,
      secret: process.env.SECRET
    })
  }


  if (process.env.ADMIN_EMAIL && process.env.ADMIN_PASSWORD) {

    try {
      const userCheck = await app.service('/api/users').find({
        query: {
          email: process.env.ADMIN_EMAIL,
        }
      })
      .catch(err => {
        console.log('setup:userCheck error', err)
      })

      console.log('userCheck', userCheck.data)

      if (userCheck.total < 1) {
        // Create test user
        console.log(`Creating Admin user ${process.env.ADMIN_EMAIL}`)
        app.service('/api/users').create({
          email: process.env.ADMIN_EMAIL,
          password: process.env.ADMIN_PASSWORD,
          permissions: ['admin'],
          // isAdmin: true
        }).catch(err => {
          console.error(`Error creating admin user ${process.env.ADMIN_EMAIL}`, err)
        })

      }
    } catch (err) {
      console.error('setup:userCheck error', err)
    }
  } else {
    console.log('Environment not set to create admin user. [ADMIN_EMAIL, ADMIN_PASSWORD]')
    if (!process.env.ADMIN_EMAIL) {
      console.error('Environment variable ADMIN_EMAIL must be defined')
      process.exit(1)
    }
  }

  try {
    const appSettings = await app.service('/api/app-settings').find()
    console.log('app-settings', appSettings)

    if (appSettings.total === 0) {
      console.log('No application settings, setting defaults')
      const defaultSettings = [
        {_id: 'allowRegistration', value: false},
        {_id: 'pruneAfterDays', value: 14},
      ]
      defaultSettings.forEach(setting => {
        app.service('/api/app-settings').create(setting)
      })

    }
  } catch (err) {
    console.warn('setup:app-settings error', err)
  }

  // Admin Settings
  try {
    const adminSettings = await app.service('/api/admin-settings').find()
    console.log('admin-settings', adminSettings)

    if (adminSettings.total === 0) {
      console.log('No admin settings, setting defaults')
      const defaultSettings = [
        {_id: 'smtpSettings'},
        {_id: 'pretend'},
      ]
      defaultSettings.forEach(setting => {
        app.service('/api/admin-settings').create(setting)
      })

    }
  } catch (err) {
    console.warn('setup:admin-settings error', err)
  }

  //
  // app.service('/api/users').create({
  //   email: 'user',
  //   password: 'user',
  //   permissions: ['user']
  // }).catch(err => {
  //   console.log(`Error creating test user`, err)
  // })

  // const result = await app.service('/api/shares').find({
  //   query: {
  //     $sort: {
  //       createdAt: '-1'
  //     },
  //     $limit: 2,
  //     createdAt: {
  //       $lt:  new Date().toISOString(),
  //     }
  //   }
  // })
  //
  // console.log('test', result, new Date().toISOString())

}

module.exports = setup
