const fs = require('fs')
const path = require('path')
const webPush = require('web-push')

const vapidKeys = webPush.generateVAPIDKeys()

const vapidKeysPath = './data/vapid-keys.json'

// Only generate if file does not exist
if(!fs.existsSync(vapidKeysPath)) {
  console.log('vapid-keys does not exist...')

  console.log('making directory')
  fs.mkdirSync(path.parse(vapidKeysPath).dir, {
    recursive: true
  })

  console.log('writing keys')
  fs.writeFileSync(vapidKeysPath,
    JSON.stringify(vapidKeys),
    'utf-8'
  )
}


