// Initializes the `appSettings` service on path `/app-settings`
const { AppSettings } = require('./app-settings.class')
const createModel = require('../../models/app-settings.model')
const hooks = require('./app-settings.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate'),
    multi: false
  }

  const appSettings = new AppSettings(options, app)
  appSettings.docs = {
    description: 'Configures public facing application settings'
  }

  // Initialize our service with any options it requires
  app.use('/api/app-settings', appSettings)

  // Get our initialized service so that we can register hooks
  const service = app.service('/api/app-settings')

  service.hooks(hooks)
}
