// Initializes the `userSettings` service on path `/user-settings`
const { UserSettings } = require('./user-settings.class')
const createModel = require('../../models/user-settings.model')
const hooks = require('./user-settings.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  }

  const userSettings = new UserSettings(options, app)

  userSettings.docs = {
    description: 'Contains user configured settings',
    // definitions: {
    //   'user-settings': {
    //     'type': 'object',
    //     'properties': {
    //       'data': {
    //         'type': 'array',
    //         'description': 'Contains an array of user configured settings'
    //       }
    //     }
    //   }
    // },
  }

  // Initialize our service with any options it requires
  app.use('/api/user-settings', userSettings)

  // Get our initialized service so that we can register hooks
  const service = app.service('/api/user-settings')

  service.hooks(hooks)
}
