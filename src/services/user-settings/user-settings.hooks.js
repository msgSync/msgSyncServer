const { authenticate } = require('@feathersjs/authentication').hooks
const { disallow, iff } = require('feathers-hooks-common')
const filterByUser = require('../../hooks/filter-by-user')
const attachUser = require('../../hooks/attach-user')
const createUserSettings = require('../../hooks/create-user-settings')

const hasNoResults = () => (context) => {
  // console.log('hasAResult', context.result.total)
  return context.result.total === 0
}

module.exports = {
  before: {
    all: [ authenticate('jwt'), filterByUser() ],
    find: [],
    get: [],
    create: [disallow('external'), attachUser()],
    update: [],
    patch: [],
    remove: [disallow('external')]
  },

  after: {
    all: [],
    find: [iff(hasNoResults(), createUserSettings())],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
