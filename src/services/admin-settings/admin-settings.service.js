// Initializes the `admin-settings` service on path `/api/admin-settings`
const {AdminSettings} = require('./admin-settings.class')
const createModel = require('../../models/admin-settings.model')
const hooks = require('./admin-settings.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate'),
    multi: false
  }

  const adminSettings = new AdminSettings(options, app)
  adminSettings.docs = {
    description: 'Configured settings that are only used on the backend.  Not publicly visible.'
  }

  // Initialize our service with any options it requires
  app.use('/api/admin-settings', adminSettings)

  // Get our initialized service so that we can register hooks
  const service = app.service('/api/admin-settings')

  service.hooks(hooks)
}
