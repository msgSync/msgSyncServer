const {authenticate} = require('@feathersjs/authentication').hooks
const {disallow, iff, isProvider, isNot, preventChanges} = require('feathers-hooks-common')
const {hashPassword, protect} = require('@feathersjs/authentication-local').hooks
const verifyHooks = require('feathers-authentication-management').hooks
const setTimestamp = require('../../hooks/set-timestamp')
const allowBySetting = require('../../hooks/allow-by-setting')
const createUserSettings = require('../../hooks/create-user-settings')
const accountService = require('../authmanagement/notifier')

const verifySignup = context => {
  accountService(context.app).notifier('resendVerifySignup', context.result)
}

const checkCreateError = context => {
  if(context.error.errorType === 'uniqueViolated') {
    throw new Error('Email is already registered')
  }
}

module.exports = {
  before: {
    all: [],
    find: [disallow('external'), authenticate('jwt')],
    get: [authenticate('jwt')],
    create: [
      iff(isNot(isProvider('server')), allowBySetting('allowRegistration')),
      hashPassword('password'),
      setTimestamp('createdAt'),
      verifyHooks.addVerification(),
    ],
    update: [disallow('external'), setTimestamp('modifiedAt'), hashPassword('password'), authenticate('jwt')],
    patch: [iff(isProvider('external'), preventChanges(true, [
      'email',
      'isVerified',
      'verifyToken',
      'verifyShortToken',
      'verifyExpires',
      'verifyChanges',
      'resetToken',
      'resetShortToken',
      'resetExpires'])),
      setTimestamp('modifiedAt'), hashPassword('password'), authenticate('jwt')],
    remove: [disallow('external'), authenticate('jwt')]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password',
        // 'isVerified',
        // 'verifyToken',
        // 'verifyShortToken',
        // 'verifyExpires',
        // 'verifyChanges',
        // 'resetToken',
        // 'resetShortToken',
        // 'resetExpires'
      )
    ],
    find: [],
    get: [],
    create: [
      createUserSettings(),
      iff(isNot(isProvider('server')), verifySignup),
      verifyHooks.removeVerification()],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [checkCreateError],
    update: [],
    patch: [],
    remove: []
  }
}
