const shares = require('./shares/shares.service.js')
const users = require('./users/users.service.js')
const files = require('./files/files.service.js')
const subscriptions = require('./subscriptions/subscriptions.service.js')
const appSettings = require('./app-settings/app-settings.service.js')
const userSettings = require('./user-settings/user-settings.service.js')
const mailer = require('./mailer/mailer.service.js')
const authmanagement = require('./authmanagement/authmanagement.service.js')
const adminSettings = require('./admin-settings/admin-settings.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(adminSettings)
  app.configure(shares)
  app.configure(users)
  app.configure(files)
  app.configure(subscriptions)
  app.configure(appSettings)
  app.configure(userSettings)
  app.configure(mailer)
  app.configure(authmanagement)
}
