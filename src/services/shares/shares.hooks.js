// A file that returns an object with all hooks that should be registered on the service.
// https://docs.feathersjs.com/guides/basics/hooks.html
const {authenticate} = require('@feathersjs/authentication').hooks
const processShare = require('../../hooks/process-share')
const setTimestamp = require('../../hooks/set-timestamp')
const sendNotification = require('../../hooks/send-notification')
const attachUser = require('../../hooks/attach-user')
const filterByUser = require('../../hooks/filter-by-user')
const coerceDate = require('../../hooks/coerce-date')
const pruneShares = require('../../hooks/prune-shares')
const attachSettings = require('../../hooks/attach-settings')
const removeFile = require('../../hooks/remove-file')

module.exports = {
  before: {
    all: [authenticate('jwt'), filterByUser()],
    find: [coerceDate('createdAt'), attachSettings()],
    get: [coerceDate('createdAt')],
    create: [setTimestamp('createdAt'), processShare(), attachUser()],
    update: [setTimestamp('modifiedAt')],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [pruneShares()],
    get: [],
    create: [sendNotification()],
    update: [],
    patch: [],
    remove: [removeFile()]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
