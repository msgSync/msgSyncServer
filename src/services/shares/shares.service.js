// Initializes the `shares` service on path `/shares`
const createService = require('feathers-nedb')
const createModel = require('../../models/shares.model')
const hooks = require('./shares.hooks')

module.exports = function (app) {
  const Model = createModel(app)
  const paginate = app.get('paginate')

  const options = {
    Model,
    paginate
  }

  const sharesService = createService(options)

  sharesService.docs = {
    description: 'A service to send text based messages',
    definitions: {
      shares: {
        'type': 'object',
        'required': [
          'text'
        ],
        'properties': {
          'type': {
            'value': 'text',
            'type': 'string',
            'description': 'The type of share [text, file]'
          },
          'text': {
            'type': 'string',
            'description': 'The message text'
          }
        }
      }
    }
  }
  // sharesService.docs = {
  //   description: 'A service to send and receive messages',
  //   definitions: {
  //     messages: {
  //       type: 'object',
  //       required: [
  //         'text'
  //       ],
  //       properties: {
  //         text: {
  //           type: 'string',
  //           description: 'The message text'
  //         },
  //         userId: {
  //           type: 'string',
  //           description: 'The id of the user that send the message'
  //         }
  //       }
  //     },
  //     'messages_list': {
  //       type: 'array',
  //       items: {
  //         $ref: `#/components/schemas/messages`
  //       }
  //     }
  //   },
  //   operations: {
  //     get: {
  //       description: 'This is my custom get description',
  //       'responses.200.description': 'Change just the description'
  //     },
  //     all: {
  //       'parameters[-]': { $ref: '#/components/parameters/customHeaderBefore' },
  //       'parameters[]': { $ref: '#/components/parameters/customHeaderAfter' },
  //       'responses.401': undefined
  //     }
  //   }
  // }

  // Initialize our service with any options it requires
  app.use('/api/shares', sharesService)

  // Get our initialized service so that we can register hooks
  const service = app.service('/api/shares')

  service.hooks(hooks)
}
