const hooks = require('./files.hooks')
const multer = require('multer')
const multipartMiddleware = multer({ limits: { fieldSize: 104857600}})
// feathers-blob service
const blobService = require('feathers-blob')
// Here we initialize a FileSystem storage,
// but you can use feathers-blob with any other
// storage service like AWS or Google Drive.
const blobFs = require('fs-blob-store')

module.exports = function (app) {
  // https://docs.feathersjs.com/guides/advanced/file-uploading.html
  // https://github.com/CianCoders/feathers-example-fileupload/blob/master/index.html

  const fileService = blobService({Model: blobFs(app.get('uploads'))})

  fileService.docs = {
    description: 'A service that handles uploading files',
    format: 'multipart/form-data',
    definitions: {
      contentType: 'multipart/form-data',  // This don't work see here to fix:  https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md#schemaObject
      format: 'multipart/form-data',
      files: {
        'type': 'object',
        // 'type': 'multipart/form-data',
        'required': [
          'type', 'text', 'fileType', 'uri'
        ],
        'properties': {
          'type': {
            'type': 'string',
            'description': 'type of share, must be: file',
            value: 'file'
          },
          'text': {
            'type': 'string',
            'description': 'name of the file'
          },
          'fileType': {
            type: 'string',
            description: 'Type of file to determine if it is an image or not'
          },
          'uri': {
            type: 'string',
            format: 'base64',
            description: 'encoded file'
          }
        }
      }
    },
  }

  app.use('/api/files',
    // multer parses the file named 'uri'.
    // Without extra params the data is
    // temporarely kept in memory
    multipartMiddleware.single('uri'),
    // another middleware, this time to
    // transfer the received file to feathers
    function (req, res, next) {
      req.feathers.file = req.file
      next()
    },
    fileService
  )

  // Initialize our service with any options it requires
  // app.use('/files', createFilesService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('/api/files')

  service.hooks(hooks)
}
