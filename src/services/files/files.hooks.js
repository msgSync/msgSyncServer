const { authenticate } = require('@feathersjs/authentication').hooks
const { disallow } = require('feathers-hooks-common')
const encodeFile = require('../../hooks/encode-file')
const getFile = require('../../hooks/get-file')
const processFile = require('../../hooks/process-file')

const removeUri = context => {
  context.result.uri = undefined
  return context
}

const setFiletype = require('../../hooks/validate-filetype');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [disallow()],
    get: [getFile()],
    create: [encodeFile(), setFiletype()],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [processFile(), removeUri],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
