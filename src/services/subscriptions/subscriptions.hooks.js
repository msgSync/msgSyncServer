const logger = require('../../logger')
const {authenticate} = require('@feathersjs/authentication').hooks

const publicKey = require('../../../data/vapid-keys').publicKey

const attachUser = require('../../hooks/attach-user')

const {disallow} = require('feathers-hooks-common')

const filterByUser = require('../../hooks/filter-by-user')


const getPublicKey = () => {
  return async context => {

    // console.log('getPublicKey')

    // Provider is the name of the transport, e.g. 'rest'
    // If undefined, this is being called internally
    if(context.params.provider) {
      context.result = {publicKey}
    }

    return context
  }
}

const log = (ctx) => {
  logger.info('Subscription Created', {...ctx.params.headers, authorization: undefined})
  return ctx
}

const processSub = async (context) => {

  const {app} = context

  // console.log('sub', context.data)

  const existingShares = await app.service('subscriptions').find({
    query: {
      endpoint: context.data.endpoint
    }
  })

  console.log('is there an existing subscriptions?', existingShares.total)
  if(existingShares.total > 0) {
    for(const sub of existingShares.data)
      app.service('subscriptions').remove(sub._id)
  }

  return context
}


module.exports = {
  before: {
    all: [authenticate('jwt'), filterByUser()],
    find: [],
    get: [],
    create: [processSub, attachUser()],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow('external')]
  },

  after: {
    all: [],
    find: [getPublicKey()],
    get: [getPublicKey()],
    create: [log],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
