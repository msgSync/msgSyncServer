// Initializes the `subscriptions` service on path `/subscriptions`
const createService = require('feathers-nedb')
const createModel = require('../../models/subscriptions.model')
const hooks = require('./subscriptions.hooks')

module.exports = function (app) {
  const Model = createModel(app)
  const paginate = app.get('paginate')

  const options = {
    Model,
    paginate
  }

  // Initialize our service with any options it requires
  app.use('/subscriptions', createService(options))

  // Get our initialized service so that we can register hooks
  const service = app.service('subscriptions')

  service.hooks(hooks)
}
