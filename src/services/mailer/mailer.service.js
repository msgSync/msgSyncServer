// Initializes the `mailer` service on path `/mailer`
const hooks = require('./mailer.hooks')
const Mailer = require('feathers-mailer')
const smtpTransport = require('nodemailer-smtp-transport')

module.exports = async function (app) {

  const adminSettings = app.service('/api/admin-settings')
  const smtpSettings = await adminSettings.get('smtpSettings')

  // Initialize our service with any options it requires
  app.use('/mailer', Mailer(smtpTransport({
    host: smtpSettings.host,
    port: smtpSettings.port,
    secure: smtpSettings.secure,
    auth: {
      user: smtpSettings.user,
      pass: smtpSettings.pass
    }
  })))

  // Get our initialized service so that we can register hooks
  const service = app.service('mailer')

  service.hooks(hooks)
}
