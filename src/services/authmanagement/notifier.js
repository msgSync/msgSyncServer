module.exports = function(app) {

  function getLink(type, hash) {
    const url = `http://${process.env.PUBLIC_URL}/${type}?token=${hash}`
    return url
  }

  function sendEmail(email) {
    return app.service('mailer').create(email).then(function (result) {
      console.log('Sent email', result)
    }).catch(err => {
      console.log('Error sending email', err)
    })
  }

  return {
    service: '/api/users',

    path: 'authManagement',

    // user: user's item, minus password.
    // notifierOptions: notifierOptions option from resendVerifySignup and sendResetPwd API calls
    notifier: function(type, user, notifierOptions) {
      console.log('notifier', type, user, notifierOptions)

      const from = `MsgSync <${process.env.ADMIN_EMAIL}>`

      let tokenLink
      let email
      if (type === 'resendVerifySignup') {//sending the user the verification email
        tokenLink = getLink('verify', user.verifyToken)
        email = {
          from,
          to: user.email,
          subject: 'Verify Signup',
          html: tokenLink
        }
        return sendEmail(email)
      } else if (type === 'verifySignup') {// confirming verification
        tokenLink = getLink('verify', user.verifyToken)
        email = {
          from,
          to: user.email,
          subject: 'Confirm Signup',
          html: 'Thanks for verifying your email'
        }
        return sendEmail(email)
      } else if (type === 'sendResetPwd') {
        tokenLink = getLink('reset', user.resetToken)
        email = {}
        return sendEmail(email)
      } else if (type === 'resetPwd') {
        tokenLink = getLink('reset', user.resetToken)
        email = {}
        return sendEmail(email)
      } else if (type === 'passwordChange') {
        email = {}
        return sendEmail(email)
      } else if (type === 'identityChange') {
        tokenLink = getLink('verifyChanges', user.verifyToken)
        email = {}
        return sendEmail(email)
      } else {
        console.warn('This should never happen in notifier....')
      }
    }
  }
}
