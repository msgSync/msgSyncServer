// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    const {data} = context

    // console.log('share data', data)
    // console.log('share params', context.params)

    // Throw an error if we don't get text
    if(!data.text) {
      throw new Error('A share must contain text')
    }

    if(!['file', 'text'].includes(data.type)) {
      throw new Error('A share must be of type "text" or "file"')
    }

    // The actual message text
    const text = context.data.text

    // Override the original data (so that people can't submit additional stuff)
    context.data = {
      text,
      type: data.type,
      file: data.file,
      isImage: data.isImage || false,
      createdAt: data.createdAt
    }

    // Best practice: hooks should always return the context
    return context
  }
}
