const allowUpsert = (context) => {
  context.params = {
    ...context.params,
    nedb: {upsert: true}
  }
  return context
}

module.exports = allowUpsert
