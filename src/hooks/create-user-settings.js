// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {

    const {app} = context

    console.log('create-user-settings', context.result)

    app.service('/api/user-settings').create({
      pruneAfterDays: 14
    }, {user: context.result})

    return context
  }
}
