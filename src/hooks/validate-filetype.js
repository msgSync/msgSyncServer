const FileType = require('file-type')
const dauria = require('dauria')
// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {

    // console.log('set-filetype', context.data.uri, context.params.file)
    if (context.data.uri){

      const parsedUri = dauria.parseDataURI(context.data.uri)
      const magicNumberFileType = await FileType.fromBuffer(parsedUri.buffer)

      if(magicNumberFileType) {
        // console.log('File Type Analyzed')
        // console.log('Analyzed: ', magicNumberFileType.mime)
        // console.log('Extension: ', magicNumberFileType.mime)
        // console.log('Presumed mediaType: ', parsedUri.mediaType)
        // console.log('Presumed MIME: ', parsedUri.MIME)
        if(magicNumberFileType.mime !== parsedUri.MIME) {
          console.warn('Presumed MIME and analyzed MIME differ')
          console.log('Presumed MIME: ', parsedUri.MIME)
          console.log('Analyzed MIME: ', magicNumberFileType.mime)
          console.log('assuming analyzed')
        }
        context.data.fileType = magicNumberFileType.mime
        context.data.isImage = magicNumberFileType.mime.startsWith('image')
      } else {
        console.log('Unable to analyze fileType')
        context.data.fileType = parsedUri.MIME
        context.data.isImage = false // Never trust the client
      }

      console.log('Determine fileType to be ', context.data.fileType)
    }

    return context
  }
}
