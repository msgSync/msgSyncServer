// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const fs = require('fs')
const path = require('path')

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    console.log('removing file', context.result)

    const shareSearch = await context.app.service('/api/shares').find({query: {file: context.result.file}})
    console.log('shareSearch', shareSearch)
    if(shareSearch.total !== 0) {
      console.log('deleting ', path.join(context.app.service('/api/files').Model.path, '..',context.result.file))
      fs.unlink(path.join(context.app.service('/api/files').Model.path, '..',context.result.file), (error) => {
        if(error) console.error(error)
      })
    }

    return context
  }
}
