// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const mapValues = require('lodash/mapValues')
const keyBy = require('lodash/keyBy')

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    const {app} = context

    let appSettings = await app.service('/api/app-settings').find()
    appSettings = mapValues(keyBy(appSettings.data, '_id'), 'value')

    // console.log('attach-settings', appSettings)

    context.params.appSettings = appSettings

    if(context.params.authenticated) {
      // console.log('user?', context.params.user)

      let userSettings = await app.service('/api/user-settings').find({query: {userId: context.params.user._id}})
      // console.log('attach-settings userSettings', userSettings)
      // console.log('attach-settings userSettings', userSettings.data[0])

      context.params.userSettings = userSettings.data[0]
    }


    return context
  }
}
