// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const errors = require('@feathersjs/errors')

// eslint-disable-next-line no-unused-vars
module.exports = (option = '') => {
  return async context => {
    const {value} = await context.app.service('/api/app-settings').get(option)
    console.log(`Looking up setting ${option}`, value)

    if(!value) {
      throw new errors.MethodNotAllowed('Application setting prevents requested action.')
    }
    if(option.length < 1) {
      throw new errors.BadRequest('No option specified for allow-by-settings hook')
    }

    return context
  }
}
