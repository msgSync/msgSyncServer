// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

const webPush = require('web-push')
const {publicKey, privateKey} = require('../../data/vapid-keys')

const ADMIN_EMAIL = process.env.ADMIN_EMAIL

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {

    const {app, params, data} = context

    // console.log('send-notification privateKey', privateKey)

    webPush.setVapidDetails(
      `mailto:${ADMIN_EMAIL}`,
      publicKey,
      privateKey
    )

    const userSub = await app.service('subscriptions')
      .find({
        query: {
          userId: params.user._id
        }
      })

    // console.log('found userSub', userSub)

    // For each user subscription, send push notification
    if(userSub.total > 0) {
      for(const sub of userSub.data) {

        webPush.sendNotification(
          sub,
          JSON.stringify({
            title: 'MsgSync',
            content: data.text.substr(0, 17),
            url: data.type === 'file' ? data.file : '/list'
          })
        )

      }
    }

    return context
  }
}
