// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const mapValues = require('lodash/mapValues')

const coerceDate = (param) => async (context) => {
  const hasParam = context.params.query[param]

  if(hasParam) {
    // console.log('hasParam', hasParam)

    switch(typeof hasParam) {
    case 'object':
      context.params.query[param] = mapValues(context.params.query[param], date => new Date(date))
      break
    case 'string':
      context.params.query[param] = new Date(hasParam)
    }

    console.log(context.params.query)
  }

  return context
}

module.exports = coerceDate
