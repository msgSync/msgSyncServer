// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const dauria = require('dauria')

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    // console.log('encodeFile', context.data, context.params.file)

    if (!context.data.uri && context.params.file){
      // console.log('getFile', context.data, context.params.file)
      const file = context.params.file
      context.data = {uri: dauria.getBase64DataURI(file.buffer, file.mimetype)}
    }

    return context
  }
}
