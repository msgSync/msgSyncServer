// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

const MILLISECONDS_PER_DAY = 1000 * 60 * 60 * 24

// a and b are javascript Date objects
function dateDiffInDays(date1, date2) {
  return Math.floor((date2 - date1) / MILLISECONDS_PER_DAY)
}

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    const {app, result: {data}, params: {appSettings, userSettings}} = context

    if(context.authenticated) {
      console.log('prune-shares appSettings', appSettings)
      console.log('prune-shares userSettings', userSettings)

      // console.log('prune-shares', context.result)
      const pruneDays = userSettings.pruneAfterDays < appSettings.pruneAfterDays ?
        userSettings.pruneAfterDays : appSettings.pruneAfterDays

      if(Array.isArray(data)) {
        const NOW = Date.now()

        data.forEach((share) => {
          // console.log('prune-shares share', share)
          // console.log('prune-shares date diff', dateDiffInDays(share.createdAt, NOW))

          if(dateDiffInDays(share.createdAt, NOW) > pruneDays) {
            console.log('Share needs to be pruned', share.createdAt)
            app.service('/api/shares')
              .remove(share._id, {user: context.params.user})
              .catch(error => console.error('Unable to remove share', error))
          }
        })
      }
    }

    return context
  }
}
