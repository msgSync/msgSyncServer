// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function filterByUser() {
  return async context => {
    // console.log('filter-by-user', {...context.params.user, password: undefined})

    if(context.params.user) {
      context.params.query = { ...context.params.query, userId: context.params.user._id }
    }

    return context
  }
}
