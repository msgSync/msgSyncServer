// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {

    const {app, result, data, params} = context

    // console.log('processing file', result)

    const existingShares = await app.service('/api/shares').find({
      query: {
        type: 'file',
        text: data.text,
        file: `/uploads/${result.id}`,
        userId: context.params.user._id
      }
    })

    // console.log('is there an existing share?', existingShares)
    if(existingShares.total > 0) {
      for(const share of existingShares.data)
        app.service('/api/shares').remove(share._id)
    }

    app.service('/api/shares').create({
      type: 'file',
      text: data.text,
      file: `/uploads/${result.id}`,
      isImage: data.isImage,
      size: result.size
    }, params)


    return context
  }
}
