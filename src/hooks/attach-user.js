// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    // The authenticated user
    const user = context.params.user

    console.log('attach-user, userid', user._id)

    context.data.userId = user._id

    // Best practice: hooks should always return the context
    return context
  }
}
