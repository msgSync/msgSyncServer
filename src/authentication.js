const { AuthenticationService, JWTStrategy } = require('@feathersjs/authentication')
const { LocalStrategy } = require('@feathersjs/authentication-local')
const { expressOauth } = require('@feathersjs/authentication-oauth')

module.exports = app => {
  const authentication = new AuthenticationService(app)

  authentication.docs = {
    description: 'Authenticate a user account and provides an Access Token',
    definitions: {
      authentication: {
        'type': 'object',
        'required': [
          'email', 'password'
        ],
        'properties': {
          'email': {
            'type': 'string',
            'description': 'user accounts email address'
          },
          'password': {
            'type': 'string',
            'description': 'password for the user account'
          }
        }
      }
    },
    operations: {
      post: {},
      delete: {}
    }
  }

  authentication.register('jwt', new JWTStrategy())
  authentication.register('local', new LocalStrategy())

  app.use('/authentication', authentication)
  app.configure(expressOauth())
}
