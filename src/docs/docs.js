const swagger = require('feathers-swagger')

const docs = swagger({
  specs: {
    info: {
      title: 'MsgSync API',
      description: 'API to interface with MsgSync server',
      version: '1.0.0'
    },
    security: {api_key: {type: 'apiKey', in: 'header', name: 'Authorization'}}
  },
  uiIndex: true,
  openApiVersion: 3,
  docsPath: '/api',
  prefix: 'api/',
  idType: 'string',
  ignore: {paths: ['mailer', 'authManagement', 'subscriptions', 'api/users']},
})

module.exports = docs
